package com.example.remindapp.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(primaryKeys = ["plan", "time"])
data class PlanEntity(
    val plan: String,
    val time: Int
)