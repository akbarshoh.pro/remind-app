package com.example.remindapp.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.remindapp.data.local.dao.PlanDao
import com.example.remindapp.data.local.entity.PlanEntity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@Database(entities = [PlanEntity::class], version = 1)
abstract class AppDB : RoomDatabase() {
    abstract fun getDao() : PlanDao
}