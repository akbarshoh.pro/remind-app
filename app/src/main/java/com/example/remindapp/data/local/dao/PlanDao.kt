package com.example.remindapp.data.local.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.Companion.REPLACE
import androidx.room.Query
import com.example.remindapp.data.local.entity.PlanEntity

@Dao
interface PlanDao {
    @Query("SELECT * FROM planentity")
    fun getAll() : List<PlanEntity>

    @Insert(onConflict = REPLACE)
    fun addPlan(planEntity: PlanEntity)

    @Delete
    fun deletePlan(planEntity: PlanEntity)

}