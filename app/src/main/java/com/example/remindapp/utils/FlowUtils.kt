package com.example.remindapp.utils

import androidx.lifecycle.LifecycleCoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

fun <T> Flow<T>.launch(scope: LifecycleCoroutineScope, block: (T) -> Unit) {
    onEach { block(it) }
        .launchIn(scope)
}