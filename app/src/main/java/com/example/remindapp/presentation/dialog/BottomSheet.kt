package com.example.remindapp.presentation.dialog

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.remindapp.databinding.DialogBottomBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class BottomSheet(
    private val text: String
) : BottomSheetDialogFragment() {
    private var _binding: DialogBottomBinding? = null
    private val binding get() = _binding!!
    private var setClick: (() -> Unit)? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = DialogBottomBinding.inflate(layoutInflater)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.appCompatTextView.text = "2 $text"
        binding.buttonSubmit.setOnClickListener {
            setClick?.invoke()
            dismiss()
        }
    }

    fun btnSave(block: () -> Unit) {
        setClick = block
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}