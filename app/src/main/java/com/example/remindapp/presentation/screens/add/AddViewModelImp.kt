package com.example.remindapp.presentation.screens.add

import androidx.lifecycle.ViewModel
import com.example.remindapp.data.local.entity.PlanEntity
import com.example.remindapp.domain.AppRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class AddViewModelImp @Inject constructor(
    private val repo: AppRepository
) : AddViewModel, ViewModel() {

    override fun addPlan(planEntity: PlanEntity) = repo.addPlan(planEntity)

}