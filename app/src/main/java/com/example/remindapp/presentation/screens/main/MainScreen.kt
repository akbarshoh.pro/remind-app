package com.example.remindapp.presentation.screens.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.work.WorkManager
import com.example.remindapp.databinding.ScreenMainBinding
import com.example.remindapp.presentation.adapter.MainAdapter
import com.example.remindapp.presentation.dialog.BottomSheet
import com.example.remindapp.utils.launch
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainScreen : Fragment() {
    private var _binding: ScreenMainBinding? = null
    private val binding get() = _binding!!
    private val viewModel: MainViewModel by viewModels<MainViewModelImp>()
    private val adapter: MainAdapter by lazy { MainAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ScreenMainBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.toAddScreen.setOnClickListener {
            findNavController().navigate(MainScreenDirections.actionMainScreenToAddScreen())
        }

//        adapter.setLong {
//            val bottomSheetDialogFragment = BottomSheet(it.plan)
//            bottomSheetDialogFragment.show(childFragmentManager, bottomSheetDialogFragment.tag)
//            bottomSheetDialogFragment.btnSave {
//                viewModel.deletePlan(it)
//                WorkManager.getInstance(requireContext()).cancelUniqueWork(it.plan + it.time.toString())
//                viewModel.getAll()
//            }
//        }

        adapter.setClick {
            viewModel.deletePlan(it)
            WorkManager.getInstance(requireContext()).cancelUniqueWork(it.plan + it.time.toString())
            viewModel.getAll()
        }

        binding.rvList.adapter = adapter
        binding.rvList.layoutManager = LinearLayoutManager(requireContext())

        viewModel.planListFlow.launch(lifecycleScope) { list ->
            if (list.isEmpty()) {
                binding.emptyState.visibility = View.VISIBLE
            } else {
                binding.emptyState.visibility = View.GONE
            }
            adapter.submitList(list)
        }

        viewModel.getAll()

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}