package com.example.remindapp.presentation.screens.main

import com.example.remindapp.data.local.entity.PlanEntity
import kotlinx.coroutines.flow.Flow


interface MainViewModel {
    val planListFlow: Flow<List<PlanEntity>>

    fun getAll()
    fun deletePlan(planEntity: PlanEntity)
}