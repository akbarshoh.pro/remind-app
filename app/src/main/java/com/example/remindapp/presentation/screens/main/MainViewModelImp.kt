package com.example.remindapp.presentation.screens.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.remindapp.data.local.entity.PlanEntity
import com.example.remindapp.domain.AppRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModelImp @Inject constructor(
    private val repo: AppRepository
) : MainViewModel, ViewModel() {
    override val planListFlow = MutableStateFlow(listOf<PlanEntity>())

    override fun getAll() {
        viewModelScope.launch {
            planListFlow.emit(repo.getAll())
        }
    }

    override fun deletePlan(planEntity: PlanEntity) = repo.deletePlan(planEntity)

}