package com.example.remindapp.presentation.screens.add

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.workDataOf
import com.example.remindapp.data.local.entity.PlanEntity
import com.example.remindapp.databinding.ScreenAddBinding
import com.example.remindapp.worker.WorkerHelper
import dagger.hilt.android.AndroidEntryPoint
import java.util.concurrent.TimeUnit

@AndroidEntryPoint
class AddScreen : Fragment() {
    private var _binding: ScreenAddBinding? = null
    private val binding get() = _binding!!
    private val viewModel: AddViewModel by viewModels<AddViewModelImp>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ScreenAddBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.back.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.addBtn.setOnClickListener {
            val plan = binding.editPlan.text.toString().trim()
            val time = if (binding.editTime.text.toString() == "") 0 else binding.editTime.text.toString().toInt()
            val tm = binding.textInputPassword
            val pn = binding.textInputEmail

            if (plan.length < 4) {
                pn.error ="Please enter a task length 5 or more."
                pn.isErrorEnabled = true
                return@setOnClickListener
            }

            if (time < 15) {
                tm.error ="Please enter a time of 15 minutes or more."
                tm.isErrorEnabled =true
                return@setOnClickListener
            }

            viewModel.addPlan(PlanEntity(plan, time))

            val data = workDataOf("title" to plan)

            val uniqueWorkName = plan + time.toLong()

            val periodicTimeRequest = PeriodicWorkRequestBuilder<WorkerHelper>(
                time.toLong(), TimeUnit.MINUTES, 5L, TimeUnit.MINUTES)
                .setInputData(data)
                .build()

            WorkManager.getInstance(requireContext())
                .enqueueUniquePeriodicWork(uniqueWorkName, ExistingPeriodicWorkPolicy.REPLACE, periodicTimeRequest)

            findNavController().popBackStack()
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}