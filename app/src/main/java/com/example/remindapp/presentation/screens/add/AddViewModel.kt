package com.example.remindapp.presentation.screens.add

import com.example.remindapp.data.local.entity.PlanEntity

interface AddViewModel {
    fun addPlan(planEntity: PlanEntity)
}