package com.example.remindapp.presentation.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.remindapp.data.local.entity.PlanEntity
import com.example.remindapp.databinding.ItemPlanBinding

class MainAdapter : ListAdapter<PlanEntity, MainAdapter.VH>(Diff) {
    private var setLong: ((PlanEntity) -> Unit)? = null
    private var setClick: ((PlanEntity) -> Unit)? = null
    private var time = System.currentTimeMillis()

    inner class VH(private val item: ItemPlanBinding) : RecyclerView.ViewHolder(item.root) {
        init {
//            item.root.setOnLongClickListener {
//                if (System.currentTimeMillis() - time > 500) {
//                    setLong?.invoke(getItem(adapterPosition))
//                    time = System.currentTimeMillis()
//                }
//                true
//            }

            item.delete.setOnClickListener {
                if (System.currentTimeMillis() - time > 500) {
                    setClick?.invoke(getItem(adapterPosition))
                    time = System.currentTimeMillis()
                }
            }
        }
        @SuppressLint("SetTextI18n")
        fun bind(data: PlanEntity) {
            val hour = data.time / 60
            val minute = data.time % 60
            val time = if (hour < 1) "$minute min" else if (minute == 0 && hour >= 1) "$hour hour" else "$hour hour $minute min"
            item.plan.text = "${adapterPosition + 1}. ${data.plan}"
            item.time.text = time
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH =
        VH(ItemPlanBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: VH, position: Int) =
        holder.bind(getItem(position))

    fun setLong(block: (PlanEntity) -> Unit) {
        setLong = block
    }

    fun setClick(block: (PlanEntity) -> Unit) {
        setClick = block
    }

    object Diff : DiffUtil.ItemCallback<PlanEntity>() {
        override fun areItemsTheSame(oldItem: PlanEntity, newItem: PlanEntity): Boolean =
            oldItem.plan == newItem.plan

        override fun areContentsTheSame(oldItem: PlanEntity, newItem: PlanEntity): Boolean =
            oldItem == newItem

    }

}