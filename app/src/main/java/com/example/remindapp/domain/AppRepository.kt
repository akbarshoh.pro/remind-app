package com.example.remindapp.domain

import com.example.remindapp.data.local.entity.PlanEntity

interface AppRepository {
    fun getAll() : List<PlanEntity>
    fun addPlan(planEntity: PlanEntity)
    fun deletePlan(planEntity: PlanEntity)
}