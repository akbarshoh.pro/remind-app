package com.example.remindapp.domain

import com.example.remindapp.data.local.dao.PlanDao
import com.example.remindapp.data.local.entity.PlanEntity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppRepositoryImp @Inject constructor(
    private val dao: PlanDao
) : AppRepository {

    override fun getAll(): List<PlanEntity> = dao.getAll()

    override fun addPlan(planEntity: PlanEntity) = dao.addPlan(planEntity)
    override fun deletePlan(planEntity: PlanEntity) = dao.deletePlan(planEntity)

}